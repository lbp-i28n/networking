# Copyright (c) 2016, 2017, 2018 Sébastien Feugère, Seb Hu-Rillettes
# Copyright (c) English edition 2008, Digital Aesthetics Research
# Center, Aarhus University, and Tatiana Bazzichelli
# Copyright (c) Italian edition 2006, costlan editori s.r.l., Milan

# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3 or
# any later version published by the Free Software Foundation; with no
# Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
# copy of the license is included in the section entitled "GNU Free
# Documentation License".
