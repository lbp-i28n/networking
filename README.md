# Networking, the Net as Network : french version

You reached a [repository](https://gitlab.com/net-as-artwork/networking/)
about a translation of the the Tatiana Bazzichelli
[*Networking, The Net as Artwork* book](http://networkingart.eu/2009/09/networking-the-net-as-artwork/).

It's available for free for [reading](https://net-as-artwork.gitlab.io/networking/) or for [downloading](https://net-as-artwork.gitlab.io/networking/book.pdf) on it's [dedicated website hosted at Gitlab pages](https://net-as-artwork.gitlab.io/networking/).

This original book is [available on archive.org](https://archive.org/details/pdfy-HIL-3u8CNVkkT-sw).

# Contributing

**We are searching for help to translate this book to french**. It can be
done from [Italian](/src/networking_it.pdf) or [English](/src/networking_bazzichelli.pdf). Any help, proof-reading, corrections and
suggestions are strongly encouraged and appreciated. See
the [contributing guide](CONTRIBUTING.md) and the [contacts section](#contacts).

# Description
The book describes the evolution of the Italian hacktivism and net
culture from the 1980s till today. It builds a reflection on the new
role of the artist and author who becomes a networker, operating in
collective nets, reconnecting to Neoavant-garde practices of the 1960s
(first and foremost Fluxus), but also Mail Art, Neoism and Luther
Blissett.

A path which began in BBSes, alternative web platforms spread in Italy
through the 1980s even before the Internet even existed, and then
moved on to Hackmeetings, to Telestreet and networking art by
different artists such as 0100101110101101.ORG, [epidemiC], Jaromil,
Giacomo Verde, Giovanotti Mondani Meccanici, Correnti Magnetiche,
Candida TV, Tommaso Tozzi, Federico Bucalossi, Massimo Contrasto,
Mariano Equizzi, Pigreca, Molleindustria, Guerriglia Marketing,
Sexyshock, Phag Off and many others.


# License

The original book, and all this work are licensed under the
[GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.html).

![Translated book cover proposal](public/icon/book_networking_bazzichelli_cover_with_translation_proposal.gif)

# Acknowledgment

Thanks to everyone who has contributed to this project in any way
including:

* Carl Schwan
* Goofy
* Seb. Hu-Rillettes
* Olivier Chardin
* Ezgi Göç
* Pierre Aubert

A special thanks to Carl Schwan who automated the publishing process.

# Contacts

* shr [ at ] balik.network
* smonff [ at ] riseup.net
