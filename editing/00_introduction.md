# Introduction

Le concept qui sous-tendrait que le réseautage est de l'art est
lourdement connoté, puisque il s'agit de l'union de deux mondes
apparemment diamétralement opposés: les pratiques du réseau et celles
de l'art. Réseauter signifie créer des réseaux de relations, de façon
à partager des expériences et des idées dans le contexte d'un échange
communicationnel, et ce dans le cadre d'une expérimentation artistique au
travers de laquelle l'expéditeur et le destinataire, l'artiste et le
public, jouent dans la même pièce.

[//]: # (Sat Jun 11 23:22:21 Je ne sais pas encore comment traduire)
[//]: # ("networking". Cela pourrait être "mise en réseau", ou "réseautage",)
[//]: # (mais "art en réseau" pourrait être pas mal aussi Sun Jun 12)
[//]: # (13:30:30 BRObert suggère "organisation réticulaire")


En Italie, au cours d'une vingtaine années d'expérimentation, et grâce
aux utilisations détournées d'Internet, un vaste réseau de personnes
partageant des objectifs politiques, culturels et artistiques a été
mis en place. Ces projets, actifs au sein de mouvements undergrounds,
intègrent des moyens d'expression variés (ordinateurs, vidéo,
télévision, radio, magazines) et des personnes investies dans des
expérimentations technologiques aussi connues sous le nom
d'*« hacktivisme »*, une terminologie couramment utilisée en Italie,
où la composante politique est essentielle. Le réseau Italien propose
une forme de critique, diffusée au travers de projets indépendants et
collectifs, partageant le dénominateur commun de la liberté
d'expression.


En même temps, cela reflète le nouveau role de l'artiste et de
l'auteur, qui devient un *« networker »*, un opérateur de réseaux
collectifs, se reconnectant lui même avec les pratiques de la
néo-avant-garde des années 60 (Fluxus ayant été la première), mais
aussi avec le mail art ou avec le néoisme et Luther Blisset.


L'art en réseau prend forme en tissant des dynamiques relationelles
ouvertes qui apparaissent spontanément, et, qui souvent, sont
difficiles à définir. Même ceux qui produisent ce type d'art (ou en
ayant produit un jour) ne définissent pas ces pratiques comme telles,
ou préfèrent plutôt ne pas le limiter à une catégorisation convenue.


Jusqu'à récemment, le terme *« art »* a rarement été utilisé pour désigner
des activités artistiques en ligne. À la place, des tentatives étaient
faites pour définir ces pratiques par la création de micro-catégories,
souvent sources d'ambiguïtés (et de vives oppositions!) même parmi les
protagonistes même. Un point de vue qui a ses racines bien enfouies
dans le rejet du concept même d'art appartement aux avant-gardes
artistiques; du dadaïsme aux futurisme et au surréalisme, et jusqu'aux
néo-avant-gardes des années 60, qui, de toute façon, ont eu lieu à
l'intérieur même des circuits de l'art.


En général, il existe une grande confusion autour des diverses
définitions, qui vont du terme net art (ou net.art, avec un point
entre net et art) au web art, hacker art, art nouveaux media, cyber
art, art électronique ou numérique.


Des termes comme net.art (avec un point entre net et art) ou hacker
art font référence à une série de pratiques et d'évènements créés
pendant des phases particulières du développement de l'approche
critique du web, comme nous le verrons dans les prochaines
pages. Cependant, parfois les définitions par catégories font prendre
le risque de délimiter un périmètre d'action basé sur la façon dont
les choses sont apparues, générant des distinctions d'appartenance
parmi celleux qui reconnaissent un terme comme plus ou moins valide qu'un
autre, sur la base d'une *« perception communautaire »* ou sur l'histoire
personnelle d'un individu.


En même temps, cela n'est plus suffisant de définir ces pratiques en
terme de medium utilisé. L'art en réseau est transversal aux arts
caractérisés par un medium de communication et de réalisation: que
celà soit par le mail art, le web art, l'art vidéo, le computer art,
le net.art, le software art, l'ASCII art, un art des media au sens
large. Pour cette raison, il est possible de dire, sans trop de se
documenter à ce propos, que le réseautage était un art et l'est bien
toujours. *Le Réseau comme Oeuvre*, bien que se libérant des
préjudices du passé et en se réappropriant un terme qui, dans son
étendue conceptuelle, lui permet d'embrasser différentes pratiques,
sans les contraindre à des formes rigides, les laissant libres de se
transformer au travers d'un entrelacement de liens qui se renouvellent
toujours. C'est de l'art en dehors du système, qui comprend, comme
nous allons le voir, de nombreuses sphères d'action.


Le terme *« art »* peut aider à se référer de façon critique à une
serie d'activités qui dépend de la construction de connexions, de
réseaux communautaires et relationnels, et ceci, entre des sujets
hétérogènes. Les travaux de réseautage cités ici permettent la
reconstruction du développement de réalités qui, depuis les années 80,
ont proposé une utilisation créative partagée, consciente de la
technologie, de la vidéo aux ordinateurs, ayant contribué à la
constitution d'une communauté de hackers Italiens. Un voyage qui va du
mail art aux BBS (réseaux informatiques alternatifs disséminés au
travers de l'Italie avant l'arrivée d'Internet), en passant par les
hackmeetings, telestreet, jusqu'aux pratiques diverses et variées
d'art en réseau et du net.art.


Le principal champ de recherche de ce livre est l'Italie, puisque
c'est ici que les pratiques de mise réseau ont déterminé la
construction d'un maillage de projets inégalé dans aucun autre
pays. Une scène avec une identité forte et sa propre sensibilité
artistique, technologique et politique a émergé dans ce pays. Les
formes d'activisme artistique (*artivisme*) et d'activisme politique
(*hacktivisme*) sont étroitement interconnectés dans un réseau étendu
diffusé partout dans le pays.


En Italie, les idées de *cyberpunk* et de *hacking* on pris pied sur une
typologie très particulière, étroitement liées à l'histoire du réseau
numérique underground et au mouvement politique radical, chose qui
n'est pas arrivée dans la plupart des autres pays. En Italie, nous
préférons utiliser le terme *« hacktivisme »* pour définir les
pratiques artistiques d'utilisation des mass-media, en leur donnant
une valeur artistique et politique qui n'est pas toujours reconnue
comme telle, en dehors de l'Italie. D'une certaine manière, notre pays
constitue un laboratoire pour l'expérimentation underground qui peut
devenir un modèle. À l'opposée, il représente aussi une sorte
*« d'île »* créative qui n'est pas toujours capable d'exporter ses
propres créations. L'idée comme quoi la *« scène Italienne »* a été
marginalisée vient directement d'ici.

Cette affirmation dérive sûrement du fait que dans de nombreux
environnements d'expérimentation, un gouffre linguistique a existé, et
existe toujours dans une certaine mesure, donc les artistes et
activistes ont eu tendance à n'utiliser que la langue Italienne et se
sont contentés de disséminer leur activités que de façon locale. Par
ailleurs, les dynamiques *« communautaires »* de l'activisme politique
ont souvent rendu les choses plus lentes. Dans tous les cas, diverses
réalités Italiennes décrites ici sont liées conceptuellement à des
réalités internationales, représentant souvent des sources
d'inspiration incontournables.


Ce livre veut mettre en lumière une série d'activités commençant dans
les années 80, en Italie et ailleurs, qui ont transformé la conception
de l'art comme objet comme un réseau de relations, en possibilités
d'intervenir personnellement et collectivement dans la création d'une
production artistique. La formation d'un art en réseau repose sur des
bases théoriques et pratiques dont les racines sont bien ancrées dans
le passé.

L'avant-garde des années 1900 avait déjà détourné son attention de
l'objet artistique pour le porter vers la vie quotidienne, en
ternissant la notion *« d'originalité »*. Mais c'est avec la
néo-avant-garde des années 60 que le public entre dans le processus de
création de l'oeuvre avec le *« happening »*, dans lequel tout le
monde est, au moins idéalement, simultanément producteur et
consommateur de l'information. Dans le *« happening »* et dans Fluxus,
l'art devient interaction, invitant le spectateur à éliminer la
distance entre lui-même et la production artistique, avec comme but de
rendre obsolète la dichotomie artiste-spectateur (elle survit
partiellement toutefois).

C'est avant tout dans les contextes extérieurs au circuit des galeries
et des musées que les possibilités d'expérimenter avec l'art comme une
interaction collective s'est vraiment matérialisée: en déplaçant le
débat depuis le domaine artistique vers la réalité sociale
quotidienne. Cela remonte aux années 70 avec les graffeurs ou le
mouvement punk et a été transmis jusqu'aux réseaux de contre-culture
numérique et le mouvement hacker actuel. Ce réseau est composé
d'activistes, d'artistes et de collectifs qui promeuvent et appliquent
l'autogestion sur l'utilisation des médias dans leur actions,
définissant l'autogestion comme auto-organisation et
auto-production des médias de communication et pratiques
artistiques.

Dans le punk, un mouvement qui constitue les bases pour beaucoup de
pratiques de l'activisme artistique et technologique en Italie, le
concept d'autogestion (DIY) se manifeste dans la volonté d'ébranler
l'opposition entre les amateurs et les professionnels, en montrant
qu'il est possible d'auto-produire son propre art (musique, fanzines,
moyens d'information, etc.), en dehors du marché.

À l'intérieur de la sphère de pratiques des artistes graffeurs, le DIY
est montré dans l'acte de personnaliser les murs autour de son lieu de
vie avec des *« tags »*[^1], activant ainsi une communication mouvante entre
diverses identités anonymes.

Le concept de DIY est aussi une clé ouvrant le développement de la
culture numérique Italienne alternative et pour les futures formes de
réseaux. La scène Italienne cyberpunk et le mouvement hacker montrent
que, en partant d'une utilisation consciente de la technologie et des
instruments du langage, on peut concevoir un type d'art dans lequel il
est possible d'intervenir personnellement: par l'activation d'un
processus de création ouvert. Ces dynamiques ont été répandues partout
en Italie au milieu des années 80, particulièrement parmi les cercles
du *Centri Sociali Occupati*[^2], développant, avec les moyens d'une
utilisation indépendante de la technologie au travers de
pratiques collectives, et avec des objectifs artistiques ainsi que
politiques, la formation d'un réseau de projets qui étaient diffusés aux
quatre coins du pays.

En même temps, vient de commencer[^3] la course à l'analyse et à la
pensée critique sur les cultures de l'Internet. Sa cime fut atteinte
au courant des années 90, ouvrant la voie aux premières
expérimentations de réalité virtuelle, forgeant les fondations de la
culture du net Italien. Ces processus ont fait figurer différents
artistes, tels que, juste pour en citer quelques un·e·s: Giacomo
Verde, Antonio Glessi et Andrea Zingoni de *Giovanotti Mondani
Meccanici*, Massimo Contrasto, Tommaso Tozzi, Federico Bucalossi,
Claudio Parrini et le collectif *Strano Network*, Simonetta Fadda, Mario
Canali et le collectif *Correnti Magnetiche*, Flavia Alman & Sabine Reiff
du collectif *Pigreca*, Helena Velena, Mariano Equizzi, en coopération avec
les éditeur·e·s de publications spécialisés telles que Francesco Galluzzi
et l'équipe éditoriale de *La Stanza Rossa*, Alessandro Ludovico de
*Neural*, le collectif de *Decoder* / *Shake Edizioni* et ceux de *Isole nella
Rete*, *FreakNet*, *AvANa.net*, *Tactical Media Crew* et différents autres
artistes, théoricien·ne·s, activistes et hackers ayant déjà été des
acteurs de l'arène de la culture numérique underground Italienne.

Certaines des personnes mentionnées ci-dessus ont développé une
attitude au delà de l'expérimentation artistique avec les médias (de
la vidéo aux ordinateurs) intégrant une forte nature communautaire,
nous faisant parler plutôt *« d'art des hackers »* ou de hacker art en
général. Avec le hacker art, la signification de l'oeuvre ne devrait
plus être vue comme sa manifestation en tant qu'objet, mais comme un
réseau de relations et de processus collectifs ayant contribué à sa
création. L'histoire des hackers en Italie est intimement tricotée à
la culture numérique et la formation des réseaux alternatifs
undergrounds.


En Italie, le concept de hacker s'est développé en même temps que le
développement d'une conscience critique sur les utilisations de la
technologie. Un hacker n'est pas seulement quelqu'un qui utilise la
technologie pour en tester ses limites, mais avant tout quelqu'un qui
croit dans la liberté d'information (et ajoutons, la liberté de l'art)
, à travers l'accessibilité de l'information et le partage de la
connaissance, qui n'a peut être jamais vu un ordinateur de sa
vie. Pour cette raison, l'éthique hacker est le positionnement
réflexif qui accompagne le mieux la plupart des pratiques décrites
ici.


Dans ce sens, les composants sociaux gagnent une importance
déterminante: c'est la raison pour laquelle, en Italie, de nombreuses
expériences entre les années 80 et 90 sont définies comme du hack
social, dans lesquelles l'expérimentation sur la technologie et la
programmation est associé à l'idée de partager ressources et
connaissances. En poursuivant cet objectif, les artistes Italiens, les
activistes et les hackers agissent concrètement en personne et sur
Internet, positionnant les pratiques de manifestation comme Netstrike
dans des sit-in virtuels, en construisant des plate-formes dédiées aux
contre-cultures et des fanzines indépendants, des laboratoires de
hackers (*hacklabs*) et en organisant des rencontres collectives orientées
sur l'auto-apprentissage et l'échange de savoirs (*hackmeetings*).


Évidemment, les projets spontanés de réseautage artistique n'existent pas
uniquement en Italie. Sur une échelle internationale, dans les dix
dernières années, les listes de diffusion, les festivals et les
projets en ligne ont été, et sont certainement toujours, un territoire
d'expérimentation primaire et de développement du net.art et, de façon
plus générale, de la *net culture* internationale. Ces expériences
ont fait figurer de nombreux théoriciens et artistes au fil des ans,
et sont fondamentaux pour le développement d'une approche critique des
réseaux. La scène du réseau en Italie se rattache elle-même avec les
pratiques hacker, de façon évidente avec le travail de l'activiste
Jaromil et, en suivant la tradition blissettienne de la participation
*« virale »* par la communication, le travail de 0100101110101101.org et
de [epidemiC], pour ne nommer que celleux qui ont eu des contacts
significatifs avec la réalité internationale.


Entre les années 90 et 2000, la culture du réseau en Italie s'est
développée au travers de la prolifération de projets collectifs qui
ont opéré dans les médias et sur Internet, étant accessibles à
tous plutôt que seulement à une poignée d'experts. De nombreux
activistes, des hackers et des artistes ont profité de la
généralisation des technologies à des prix abordables pour tous, des
ordinateurs aux caméras vidéo, pour réaliser des projets d'une grande
portée et fournissant ainsi un tournant décisif pour la culture du
réseau Italien en termes de diffusions dans le pays.


De nouvelles réalités sont apparues: telles que *Indymedia* Italie, le
réseau *Telestreet*, le collectif *New Global Vision*, et le serveur libre
*Autistici/Inventari*, iels ont expérimenté avec les ordinateurs, la
vidéo et Internet toujours avec un point de vue critique. De nombreux
autres projets locaux sont liés à ces réalités nationales avec
lesquelles ils partagent les même objectifs politiques et
technologiques. Parmi eux, les collectifs: *Candida TV*, *Serpica Naro*,
*Molleindustria* et d'autres groupes répondant activement aux
problématiques sociales du post-néolibéralisme, telles que
ChainWorkers et dans le domaine des stratégies marketing, les
tactiques de Guerrigliamarketing.it.


La majorité des projets susmentionnés sont apparus après le
contre-sommet du G8 de 2001 à Genève; une étape non seulement qui a
fait date en terme d'affrontements difficiles, de répression et de
violence, comme la majorité des médias officiels l'ont souligné, mais
aussi comme une importante expérience pour quiconque construit une
information de proximité, avec des caméras vidéo amateur, des sites
internet underground ou des radios libres. Les trois jours à Gênes ont
non seulement assené un coup dur aux groupes d'activistes Italiens au
travers de la violence et de la répression, mais ont aussi contribué à
forger une réflexion critique plus incisive sur les médias, la
technologie et les formes d'activisme politique.


[//]: # (Tue Jun 14 18:33:05 Tactical frivolity reste à définir, je n'ai pas)
[//]: # (trouvé de terme français)
[//]: # (https://en.wikipedia.org/wiki/Tactical_frivolity)
[//]: # (https://fr.wikipedia.org/wiki/Clandestine_Insurgent_Rebel_Clown_Army)

Conjointement au projet de réseau décrit ci-dessus, il y a également
des organisations répondent aux stratégies de l'opposition avec la
*« frivolité ludico-tactique »*[^4], mettant le corps de chacun en jeu
et en subvertissant l'idée d'action politique comme une
*« resistance »*: ce qui a montré ses forces comme ses limites à
Gênes. Dans ce réseau, le corps devient un canal fondamental au
travers duquel il est possible de créer de nouvelles ouvertures, pour
initier des expérimentations sur les *« marges »* - même sur des
territoires sexuels. Il y a de nombreux projets qui aspirent à former
un réseaux d'envergure, principalement décrit par le terme *« queer »* ou
*« pink »*. Parmi eux, se détachent le collectif *Sexyshock* à Bologne,
*Phag-Off* à Rome et les *Pornflakes* à Milan, dont l'activité est insérée
dans un discours de réflexion sur la sexualité, la pornographie et les
expérimentations artistiques allant du cyberféminisme au netporn et à
l'indie-porn sur Internet.


La description des liens existant dans ce dynamique réseau Italien
d'expérimentations artistiques, technologiques et politiques, vise à
montrer comment il est possible de créer des chemins impliquant des
canaux *« alternatifs »*, comparé à ceux qui dominent l'économie de
marché, par des politiques de contrôle et par des sources
d'informations liées à des entités commerciales, souvent présentées
dans les sociétés occidentales comme les seules possibilités
disponibles. Une critique du *status quo* environnant, pas seulement
sous l'angle social et politique, mais aussi sous l'angle artistique,
comme c'est arrivé par exemple, avec l'épisode de la *Conspiration de
Tirana*[^5] au début du 21ème siècle. Cet évènement, que peu de personnes
connaissent, porta un coup dur au système Italien, qui est basé sur
des dynamiques commerciales. La *Conspiration de Tirana* illustre
l'absurdité du système artiste-marché-collectionneur, et montre que
les challenges artistiques actuels portent sur l'invention de
nouvelles solutions d'action et de nouveaux types de contenu.

Les projets de réseau décrits dans ce livre agissent dans ces espaces,
dans les fractures sociales et culturelles qui se tiennent
manifestement aux marges de la vie quotidienne, mais qui constituent
en réalité un important territoire de réinvention et de réécriture des
symboles et des codes d'expression avec lesquels on transforme et
décode notre présent. Il n'est pas surprenant donc, que Internet,
outil que nous utilisons tous dans notre travail aujourd'hui, soit le
fruit de connexions, de batailles et de relations où les hackers
tiennent un rôle prépondérant. De la même façon, ce n'est pas
surprenant que de nombreuses personnes mentionnées ici comme
précurseurs de différents processus artistiques et culturels aient
aussi contribué à l'imaginaire Italien actuel dans le domaine des mass
media et de la technologie.

Ce livre veut redonner leurs justes poids et valeur aux nombreuses
pratiques artistiques, sociales et culturelles qui, au travers de leur
réseau de relations politiques, artistiques, technologiques et
affectives, ont contribué à construire les instruments fascinants que
nous utilisons aujourd'hui au quotidien, de l'ordinateur personnel à
Internet. En espérant que les expériences décrites ici puissent
devenir un modèle pour tou·te·s celleux qui souhaiteraient
continuer à travailler de façon créative dans les espaces et les
fractures du quotidien, ou pour celleux qui combattront pour
s'assurer que tout ceci reste ouvert, permettant à chacun·e de se
l'approprier à nouveau.

Évidemment, les composantes de réseautage personnel jouent un rôle
central dans des sujets tels que ceux-ci. Mon analyse devrait par
conséquent être vue comme une possibilité parmi d'autres, et j'espère
que d'autres points de vue suivront, dictés par les mondes personnels
que nous avons tous bâtis, suivant les dynamiques de réseau de
chacun. J'accueille un futur de nouvelles connections et de partage;
ouvrant le contenu de ce livre à des réflexions futures et des
re-élaborations, avec une passion commune, l'enthousiasme et même
ĺ'idéalisme qui a caractérisé ces vingt dernières années dans
l'histoire du réseau Italien.

[^1]: Penser ici au lien entre le tag sur les mur de la ville et au principe de *« tag »* sur Internet évoqué en préface du livre, N.d.T.

[^2]: *Centre Sociaux Squattés*

[^3]: À la date de la rédaction de ce livre en 2006, N.d.T.

[^4]: *« ludic-tactic frivolity »*, d'après *tactical frivolity*, une pratique de manifestation faisant appel à l'humour, à la désobéisance pacifique aux autoritées, des déguisement et autres performances fantaisistes, N.d.T.

[^5]: C.f. chapitre 5 *Art on the Net and for the Net*, N.d.T.
